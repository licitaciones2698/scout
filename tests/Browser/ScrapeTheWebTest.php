<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ScrapeTheWebTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function test_I_can_login_successfully()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/login')
                ->assertSee('Estamos en logueo')
                ->type('email', 'alejoandk@gmail.com')
                ->type('password', 'alejo123')
                ->press('Login')
                ->assertPathIs('/home');
        });

    }
}
