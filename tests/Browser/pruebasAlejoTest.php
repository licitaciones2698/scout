<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
// use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\User;

class preubasAlejo extends DuskTestCase
{
    // use DatabaseMigrations;
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function test_se_puede_el_vue()
    {

        // uso de vue y de tiempo de espera
        // mostrar la consola
        // $user = factory(User::class)->create(); use ($user)
        $this->browse(function (Browser $browser){
            $browser->visit('/busqueda')
            ->assertSee('Municipio')
            ->value('.card-header' , 'Estamos dentro de busqueda')
            ->type('titulo', 'Caldas')
            ->press('busq-city')
            ->assertVue('titulo', 'Caldas', '@profile-component')
            ->waitForText('Manizales')
            ->screenshot('create-status-3');

        });
    }

    // public function test_se_puede_otra_funcion()
    // {
    //     $this->browse(function (Browser $browser){
    //         $browser->visit('/busqueda')
    //         ->assertSee('Municipio')
    //         ->screenshot('create-status-4');
    //     });
    // }
}
