<?php

namespace App\Listeners;

use App\Events\PaisInfectadoEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Log;
use App\noticias;

class EnvioNoticiaListener implements ShouldQueue
{

    use InteractsWithQueue;
    public $tries = 1;
    /**
     * Handle the event.
     *
     * @param   $event
     * @return void
     */
    public function handle($event)
    {
        $noticia_nueva = new noticias;
        $noticia_nueva->noticia = 'Ultima hora se infecto la ciudad ' . $event->ciudad;
        if($noticia_nueva->save()){
            Log::error('Guarde correctamente la noticia');
        }
    }


    public function failed($event, $exception)
    {
       Log::error('Hubo un error ');
    }

    // public function shouldQueue($event)
    // {
    //     return true;
    // }
}
