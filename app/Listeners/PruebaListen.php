<?php

namespace App\Listeners;

use App\Events\pruebasEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class PruebaListen
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  pruebasEvent  $event
     * @return void
     */
    public function handle(pruebasEvent $event)
    {
        //
    }
}
