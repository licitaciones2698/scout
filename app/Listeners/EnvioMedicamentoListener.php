<?php

namespace App\Listeners;

use App\Events\PaisInfectadoEvent;
use Illuminate\Queue\InteractsWithQueue;
// use Illuminate\Contracts\Queue\ShouldQueue;
use Log;

class EnvioMedicamentoListener
{
    use InteractsWithQueue;

    /**
     * Handle the event.
     *
     * @param  PaisInfectadoEvent  $event
     * @return void
     */
    public function handle(PaisInfectadoEvent $event)
    {
        Log::error('Entre al listener de envioayuda ' . $event->ciudad );
    }
}
