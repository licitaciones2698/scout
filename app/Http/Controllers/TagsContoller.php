<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use TeamTNT\TNTSearch\TNTSearch;
use App\tags;
use App\Events\PaisInfectadoEvent;

class TagsContoller extends Controller
{
   
	public function Index(){
		return view('busqueda');
	}

	public function Traer(Request $request){
		$tnt = new TNTSearch;
		$tnt->loadConfig([
		    'driver'    => 'mysql',
		    'host'      => 'localhost',
		    'database'  => 'world',
		    'username'  => 'root',
		    'password'  => '',
		    'storage'  => storage_path(),
		]);

		$tnt->selectIndex("city_index.index");
		$res = $tnt->search($request->input('titulo'));
		$resultado= tags::whereIn('Id',$res['ids'])->select('Name', 'ID', 'Population', 'CountryCode')->get();
		return response()->json(['ciudad' => $resultado , 'datos' => $res]);
	}

	public function Destruir(Request $request){
		$ciudad = tags::where('ID' , $request->input('id'))->delete();
		$nombre = $request->input('nombre');
	 	event(new PaisInfectadoEvent($nombre));
		return response()->json(['mensaje' => 'Destruido']);
	}
}
