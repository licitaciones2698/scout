<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;
class tags extends Model
{

	protected $primaryKey = "ID";
	protected $table = "city";
    use Searchable;

    public function SearchableAs()
    {
        return 'city_index';
    }
}
