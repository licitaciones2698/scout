<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class noticias extends Model
{
   	protected $primaryKey = "id";
	protected $table = "noticias";
	public $timestamps = false;
	// public $incrementing = true;
}
