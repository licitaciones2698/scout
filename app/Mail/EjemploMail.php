<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EjemploMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The order instance.
     *
     * @var Order
     */
    public $nombre = Null;
    public $nombre2 = Null;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($prueba)
    {
        $this->prueba = 'Alejo';
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('maileclipse::templates.ejmplosTemplates');
    }
}
