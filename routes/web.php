<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/', function () {
    return view('welcome');
});
Route::get('/busqueda', 'TagsContoller@Index');
Route::post('/tags', 'TagsContoller@Traer');
Route::post('/destruir', 'TagsContoller@Destruir');
Route::get('/home', 'HomeController@index')->name('home');

// Rutas para password
Route::get('/vista-password', function(){
	return view('password_grant');
});
Route::get('/vista-cliente', function(){
	return view('client_grant');
});
Route::get('/vista-personal', function(){
	return view('personal_grant');
});
Route::get('/admin-crear-cliente', function(){
	return view('admin_cliente');
});


// Route::post('/admin-crear', 'HomeController@loginpru');



// Route::get('/vista-password', 'HomeController@PasswordView');
// Route::get('/vista-cliente', 'HomeController@ClientView');
