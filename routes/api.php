<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('auth:api')->get('mundos', function (Request $request) {
    return App\tags::all();
});

Route::middleware('client')->get('munditos', function (Request $request) {
    return App\tags::all();
});

Route::middleware('auth:api')->get('mundo-personal', function (Request $request) {
    return App\tags::all();
});

Route::group(['middleware' => ['auth:api' , 'scope:get-tur']], function() {
	Route::get('post-scopes' ,function (Request $request) {
   		return App\tags::where('CountryCode' , 'TUR')->get();
	});
});

Route::group(['middleware' => ['auth:api' , 'scope:get-two-post']], function() {
	Route::get('post-scopes-dos' ,function (Request $request) {
   		return App\tags::limit(2)->get();
	});
});

Route::post('/admin-crear', 'HomeController@loginpru');