@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
        	<passport-clients></passport-clients>
            <passport-personal-access-tokens></passport-personal-access-tokens>
            {{-- <div>
                <form role="form" action="{{url('/oauth/clients')}}">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label">Nombre</label>
                        <div class="col-md-9">
                            <input id="username" type="text" class="form-control" name="name">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label">Redirección</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="password" name="redirect">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label">Crear</label>
                        <div class="col-md-9">
                            <input type="submit" class="btn btn-primary" name="send" value="Crear">
                        </div>
                    </div>
                </form>
            </div> --}}
            <passport-authorized-clients></passport-authorized-clients>
        </div>
    </div>
</div>
@endsection
