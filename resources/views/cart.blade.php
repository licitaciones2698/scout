<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <title>Neopolitan Progress Email</title>
  <!-- Designed by https://github.com/kaytcat -->
  <!-- Robot header image designed by Freepik.com -->

  <style type="text/css">



  /*
>>>>>>>>> CHANGING PROGRESS STEP IMAGES <<<<<<<<<<<<<<<

-If you would like to change the progress step images from 3 to any other step please view the html comments below.
-You just need to switch the provided image url's.


*/





  @import  url(http://fonts.googleapis.com/css?family=Droid+Sans);

  /* Take care of image borders and formatting */

  img {
    max-width: 600px;
    outline: none;
    text-decoration: none;
    -ms-interpolation-mode: bicubic;
  }

  a {
    text-decoration: none;
    border: 0;
    outline: none;
    color: #bbbbbb;
  }

  a img {
    border: none;
  }

  /* General styling */

  td, h1, h2, h3  {
    font-family: Helvetica, Arial, sans-serif;
    font-weight: 400;
  }

  td {
    text-align: center;
  }

  body {
    -webkit-font-smoothing:antialiased;
    -webkit-text-size-adjust:none;
    width: 100%;
    height: 100%;
    color: #37302d;
    background: #ffffff;
    font-size: 16px;
  }

   table {
    border-collapse: collapse !important;
  }

  .headline {
    color: #ffffff;
    font-size: 36px;
  }

 .force-full-width {
  width: 100% !important;
 }

 .step-width {
  width: 110px;
  height: 111px;
 }



  </style>

  <style type="text/css" media="screen">
      @media  screen {
         /*Thanks Outlook 2013! http://goo.gl/XLxpyl*/
        td, h1, h2, h3 {
          font-family: 'Droid Sans', 'Helvetica Neue', 'Arial', 'sans-serif' !important;
        }
      }
  </style>

  <style type="text/css" media="only screen and (max-width: 480px)">
    /* Mobile styles */
    @media  only screen and (max-width: 480px) {

      table[class="w320"] {
        width: 320px !important;
      }

      img[class="step-width"] {
        width: 80px !important;
        height: 81px !important;
      }


    }
  </style>
</head>
<body class="body" style="padding:0; margin:0; display:block; background:#ffffff; -webkit-text-size-adjust:none" bgcolor="#ffffff">
<table width="100%" cellspacing="0" cellpadding="0" align="center">
<tbody>
<tr>
<td align="center" valign="top" bgcolor="#ffffff" width="100%"><center>
<table class="w320" style="margin: 0 auto;" width="600" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td align="center" valign="top">
<table style="margin: 0 auto;" width="100%" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td style="font-size: 30px; text-align: center;"><br />Awesome Co <br /><br /></td>
</tr>
</tbody>
</table>
<table style="margin: 0 auto;" width="100%" cellspacing="0" cellpadding="0" bgcolor="#4dbfbf">
<tbody>
<tr>
<td class="headline"><br />pruebas alejo2</td>
</tr>
<tr>
<td><img class="step-width" style="font-family: 'Droid Sans', 'Helvetica Neue', Arial, sans-serif;" src="https://www.filepicker.io/api/file/mepNOdHRTCMs1Jrcy2fU" alt="step three" /><br /><center>
<table class="force-width-80" style="margin: 0 auto;" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td>&nbsp;</td>
<!-- ======== STEP TWO ========= --> <!-- ==== Please use this url: https://www.filepicker.io/api/file/QKOMsiThQcePodddaOHk in the src below in order to set the progress to two.

                            Then replace step three with this url: https://www.filepicker.io/api/file/qnkuUNPS6TptLRIjWERA

                            Then replace step one with this url: https://www.filepicker.io/api/file/MMVdxAuqQuy7nqVEjmPV ==== -->
<td>&nbsp;</td>
<!-- ======== STEP THREE ========= --> <!-- ==== Please use this url: https://www.filepicker.io/api/file/mepNOdHRTCMs1Jrcy2fU in the src below in order to set the progress to three.

                            Then replace step one with this url: https://www.filepicker.io/api/file/MMVdxAuqQuy7nqVEjmPV

                            Then replace step two with this url: https://www.filepicker.io/api/file/MD29ZQs3RdK7mSu0VqxZ ==== -->
<td>&nbsp;</td>
</tr>
<tr>
<td style="vertical-align: top; color: #187272; font-weight: bold;">One</td>
<td style="vertical-align: top; color: #187272; font-weight: bold;">Two</td>
<td style="vertical-align: top; color: #187272; font-weight: bold;">Done!</td>
</tr>
</tbody>
</table>
<img class="step-width" style="font-family: 'Droid Sans', 'Helvetica Neue', Arial, sans-serif;" src="https://www.filepicker.io/api/file/MMVdxAuqQuy7nqVEjmPV" alt="step one" /></center></td>
</tr>
<tr>
<td><center>
<table style="margin: 0 auto;" width="60%" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td style="color: #187272;"><br /><br />Your account has successfully been updated! Click below to review or update changes! <br /><br /></td>
</tr>
</tbody>
</table>
</center></td>
</tr>
<tr>
<td>
<div><!-- [if mso]>
                        <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="http://" style="height:50px;v-text-anchor:middle;width:200px;" arcsize="8%" stroke="f" fillcolor="#178f8f">
                          <w:anchorlock/>
                          <center>
                        <![endif]--> <a style="background-color: #178f8f; border-radius: 4px; color: #ffffff; display: inline-block; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: bold; line-height: 50px; text-align: center; text-decoration: none; width: 200px; -webkit-text-size-adjust: none;" href="http://127.0.0.1">My Account</a> <!-- [if mso]>
                          </center>
                        </v:roundrect>
                      <![endif]--></div>
<br /><br /></td>
</tr>
</tbody>
</table>
<table style="margin: 0px auto; height: 343px; width: 100%;" width="100%" cellspacing="0" cellpadding="0" bgcolor="#f5774e">
<tbody>
<tr style="height: 86px;">
<td class="headline" style="background-color: #f5774e; height: 86px;"><br />New Features</td>
</tr>
<tr style="height: 93px;">
<td style="height: 93px;"><img style="background-color: #ffffff; font-family: 'Droid Sans', 'Helvetica Neue', Arial, sans-serif;" src="https://www.filepicker.io/api/file/tjUsYjIHSDCkrrniLuev" alt="meter image" width="145" height="89" /></td>
</tr>
<tr style="height: 76px;">
<td style="height: 76px;"><center>
<table style="margin: 0px auto; height: 82px; width: 0.833333%;" width="5" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td style="color: #933f24;"><br />Faster download speeds for all users! If speed is what you need, we&lsquo;ve got you covered! <br /><br /></td>
</tr>
</tbody>
</table>
</center></td>
</tr>
<tr style="height: 88px;">
<td style="height: 88px;">
<div><a style="background-color: #ac4d2f; border-radius: 4px; color: #ffffff; display: inline-block; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: bold; line-height: 50px; text-align: center; text-decoration: none; width: 200px; -webkit-text-size-adjust: none;" href="http://127.0.0.1">Learn More</a> <!-- [if mso]>
                          </center>
                        </v:roundrect>
                      <![endif]--></div>
<br /><br /></td>
</tr>
</tbody>
</table>
<table class="force-full-width" style="margin: 0 auto;" cellspacing="0" cellpadding="0" bgcolor="#414141">
<tbody>
<tr>
<td style="background-color: #414141;"><br /><br /><img src="https://www.filepicker.io/api/file/R4VBTe2UQeGdAlM7KDc4" alt="google+" /> <img src="https://www.filepicker.io/api/file/cvmSPOdlRaWQZnKFnBGt" alt="facebook" /> <img src="https://www.filepicker.io/api/file/Gvu32apSQDqLMb40pvYe" alt="twitter" /> <br /><br /></td>
</tr>
<tr>
<td style="color: #bbbbbb; font-size: 12px;"><a href="#">View in browser</a> | <a href="#">Unsubscribe</a> | <a href="#">Contact</a> <br /><br /></td>
</tr>
<tr>
<td style="color: #bbbbbb; font-size: 12px;">&copy; 2014 All Rights Reserved <br /><br /></td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</center></td>
</tr>
</tbody>
</table>
</body>
</html>