
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html;" charset="utf-8" />
      <!-- view port meta tag -->
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
      <!--Outlook Express procesa las secuencias de comandos del encabezado en HTML en contenido HTML. El control MSHTML ejecuta automáticamente estas secuencias de comandos.-->
      <META content="MSHTML 6.00.2900.2963" name=GENERATOR>
      <title>Cliente</title>
      <style type="text/css">
         /* hacks */
         * { -webkit-text-size-adjust:none; -ms-text-size-adjust:none; max-height:1000000px;}
         table {border-collapse: collapse !important;}
         #outlook a {padding:0;}
         .ReadMsgBody { width: 100%; }
         .ExternalClass { width: 100%; }
         .ExternalClass * { line-height: 100%; }
         .ios_geo a { color:#1c1c1c !important; text-decoration:none !important; }
         /* responsive styles */
         @media only screen and (max-width: 600px) { 
         /* global styles */
         .hide{ display:none !important; display:none;}
         .blockwrap{ display:block !important; }
         .showme{ display:block !important; width: auto !important; overflow: visible !important; float: none !important; max-height:inherit !important; max-width:inherit !important; line-height: auto !important; margin-top:0px !important; visibility:inherit !important;}
         *[class].movedown{ display: table-footer-group !important;}
         *[class].moveup{ display: table-header-group !important; }
         /* font styles */
         *[class].textright{ text-align: right !important; }
         *[class].textleft{ text-align: left !important; }
         *[class].textcenter{ text-align: center !important; }
         *[class].font27{ font-size: 27px !important; font-weight:normal !important; line-height:27px !important; }
         /* width and heights */
         *[class].hX{ height:Xpx !important; }
         *[class].w380{ width:380px !important; text-align: justify; }
         *[class].w360{ width:360px !important; }
         *[class].w175{ width:175px !important; }
         *[class].w350{ width:350px !important; }
         /* spacios */
         *[class].mtop10{ margin-top: 10px !important; }
         *[class].mtop20{ margin-top: 20px !important; }
         *[class].mbt10{ margin-bottom: 10px !important; }
         *[class].mbtn20{ margin-bottom: 20px !important; }
         *[class].pleft15{ padding-left: 15px !important; text-align: justify; }
         *[class].pright15{ padding-right: 15px !important; text-align: justify; }
         }
      </style>
   </head>
   <body style="margin:0; padding:0;" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
      <div style="display:none;font-size:1px;color:#FFFFFF;line-height:1px;max-height:0px;max-width:0px;opacity:0;overflow:hidden;">
         Cliente
      </div>
      <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
         <tr>
            <td align="center" valign="top">
               <!-- tabla de contenido -->
               <table width="640" border="0" align="center" cellpadding="0" cellspacing="0" class="w380">
                  <tr>
                     <td width="640" class="w380">
                        <table>
                           <tr>
                              <td style="height: 18px;"></td>
                           </tr>
                        </table>
                        <table width="640" border="0" align="center" cellpadding="0" cellspacing="0" class="w380">
                           <tr>
                              <td width="640" class="w380 textcenter">
                                 <b><span style="font-family: arial; font-size:20px; line-height:20px; color: #595959;">Licitaci</span><span style="font-family: arial; font-size:20px; line-height:20px; color: #00b0f0;">o</span><span style="font-family: arial; font-size:20px; line-height:20px; color: #595959;">nes</span><span style="font-family: arial; font-size:20px; line-height:20px; color: #00b0f0;">.info</span></b>
                              </td>
                           </tr>
                        </table>
                        <table>
                           <tr>
                              <td height="5" style="height: 5px;"></td>
                           </tr>
                        </table>
                        <!-- contenedor principal -->
                        <!-- borde de arriba -->
                        <table width="640" border="0" cellpadding="0" cellspacing="0" class="w380" cellmargin="0">
                           <tr>
                              <td width="10" style="width:10px;"></td>
                              <td width="620" style="width:620px;" bgcolor="#ffffff" class="w360"></td>
                              <td width="10" style="width:10px;"></td>
                           </tr>
                        </table>
                        <!-- spacer -->
                              <!--Los comentarios condicionales facilitan la detección de versiones anteriores de Windows Internet Explorer-->
                                <!--[if (gte mso 9)|(IE)]>
                                 <table width="640" align="center" cellpadding="0" cellspacing="0" border="0">
                                 <tr>
                                     <td>
                              <![endif]-->
                        <table width="640" border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff" class="w380" cellmargin="0">
                           <tr>
                           
                              <td height="10" width="610" style="width:610px; height: 10px;" class="w380">
                              </td>
                           </tr>
                        </table>
                         <!--[if (gte mso 9)|(IE)]>
                                   </td>
                               </tr>
                           </table>
                           <![endif]-->
 

                        <!-- linea separadora -->
                        <table width="640" border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff" class="w380 hide" cellmargin="0">
                           <tr>
                              <td width="15">
                              </td>
                              <td height="1" style="line-height:1px; font-size:1px;" bgcolor="#0d133d" width="610"></td>
                              
                              </td>
                              <td width="15">
                              </td>
                           </tr>
                        </table>
                        <!-- spacer -->
                        <table width="640" border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff" class="w380" cellmargin="0">
                           <tr>
                              <td width="610" height="20" style="width:610px; height: 20px;" class="w380">
                              </td>
                           </tr>
                        </table>
                        <!-- hero -->
                        <table width="640" border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff" class="w380" cellmargin="0">
                           <tr>
                              <td width="15" class="hide">&nbsp;</td>
                              <td width="600">
                                 <table width="600" border="0" cellpadding="0" cellspacing="0" class="w380">
                                    <tr>
                                       <td width="600" style="text-align: justify;" class="w380 pleft15 pright15">
                                          <table width="640" border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff" class="w380" cellmargin="0">
                                             <tr>
                                                <td height="15" style="width:610px; height: 15px;" class="w380">
                                                </td>
                                             </tr>
                                          </table>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td width="600" style="text-align: justify;" class="w380 pleft15 pright15">
                                          <span style="font-family: arial; font-size:15px; line-height:25px; color: #606060;">
                                            
                                             <span style="width:100%;color: #606060;text-align: left;">
                                                Contacto por web de <b>{{ $Nombre }}</b><br>
                                                <b>Email: </b>{{ $Email }}<br>
                                                <b>Mensaje: </b>{{ $Mensaje }}
                                                @if(!empty($Telefono))
                                                   <br><b>Telefono: </b>{{ $Telefono }}
                                                @endif
                                                @if(!empty($Ciudad))
                                                   <br><b>De la ciudad: </b>{{ $Ciudad }}
                                                @endif
                                             </span>
                                          </span>
                                       </td>
                                    </tr>
                                 </table>
                              </td>
                           </tr>
                        </table>
                        <!-- end hero -->
                        <table width="640" border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff" class="w380" cellmargin="0">
                           <tr>
                              <td width="610" height="20" style="width:610px; height: 20px;" class="w380">
                              </td>
                           </tr>
                        </table>
                        <table width="640" border="0" cellpadding="0" cellspacing="0" bgcolor="#0d133d" class="w380" align="center"  cellmargin="0">
                           <tr>
                              <td width="640" height="1" style="width: 610px; height: 1px;" class="w380 hide">
                              </td>
                           </tr>
                        </table>
                        <table width="640" border="0" cellpadding="0" cellspacing="0" bgcolor="#EBEDEF" class="w380" cellmargin="0" align="center">
                           <tr>
                              <td width="610"  height="40" style="width: 610px; height: 40px;" class="w380 textcenter" align="center">
                                 <span style="font-family: arial; font-size:17px; line-height:30px; color: #595959;"><b>Descarga nuestra App movil para que estes informado casi en tiempo real</b></span>
                              </td>
                           </tr>
                        </table>
                        <table width="640" border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff" align="center" class="w380" cellmargin="0">
                           <tr>
                              <td align="center" height="20" width="610" style="height: 20px; width: 610px;" class="w380">
                                 <table width="60%" align="center">
                                    <tr>
                                       <td height="30" style="width: 28%; height: 30px;"></td>
                                       <td height="30" style="width: 4%;height: 30px;"></td>
                                       <td height="30" style="width: 28%;height: 30px;"></td>
                                    </tr>
                                    <tr>
                                       <td align="center" height="55" width: 28%; bgcolor="#0F0F0F" align="center" style="font-family: arial; font-size:18px; color:#0d133d; line-height:25px; border-radius: 5px 5px 5px 5px;"><a href="https://play.google.com/store/apps/details?id=com.setcon.licitacionesinfo" style="color:#ffffff; text-decoration:none;"><b>Google play</b></a></td>
                                       <td style="width: 4%;"></td>
                                       <td align="center" height="55" width: 28%; bgcolor="#0F0F0F" align="center" style="font-family: arial; font-size:18px; color:#0d133d; line-height:25px; border-radius: 5px 5px 5px 5px;"><a href="https://itunes.apple.com/us/app/licitaciones/id1210052711?mt=8" style="color:#ffffff; text-decoration:none;"><b>Apps Store</b></a></td>
                                    </tr>
                                 </table>
                              </td>
                           </tr>
                        </table>
                        <!-- footer -->
                        <!-- spacer  -->
                        <table width="640" border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff" align="center" class="w380" cellmargin="0">
                           <tr>
                              <td align="center" height="20" width="610" style="height: 20px; width: 610px;" class="w380">
                           <tr>
                              <td align="center" style="font-family: arial; font-size:15px; color:#0d133d; line-height:20px;">
                                 <b>Conectate con nosotros</b><br>
                                 <table width="180" align="center">
                                    <tr>
                                       <td align="center">
                                          <table width="90" align="center">
                                             <tr>
                                                <td width="20" height="20" bgcolor="#3b5998" align="center" style="font-family: tahoma; font-size:14px;  border-radius: 5px 5px 5px 5px; width: 20px; height: 20px;"><a href="https://www.facebook.com/licitacionesInfo/" style="color:#ffffff; text-decoration:none;"><b>f</b></a></td>
                                                <td style="width: 5px;"></td>
                                                <td width="20" height="20" bgcolor="#48aae6" align="center" style="width: 20px; height: 20px; font-family: tahoma; font-size:14px; border-radius: 5px 5px 5px 5px;"><b><a href="https://twitter.com/licitacionesInf/" style="color:#ffffff; text-decoration:none;">t</a></b></td>
                                                <td style="width: 5px;"></td>
                                                <td align="center" width="20" height="20" bgcolor="#dc4a38" style="font-family: arial; width: 20px; height: 20px; font-size:12px; border-radius: 5px 5px 5px 5px;"><a href="https://plus.google.com/+LicitacionesInfo" style="color:#ffffff; text-decoration:none;"><b>G+</b></a></td>
                                             </tr>
                                          </table>
                                       </td>
                                    </tr>
                                 </table>
                              </td>
                           </tr>
                           </td>
                           </tr>
                        </table>
                        <table align="center" width="640" border="0" cellpadding="0" cellspacing="0" class="w380" cellmargin="0">
                           <tr>
                              <td align="center" height="20" width="610" style="height: 20px; width: 610px;" class="w380">
                              </td>
                           </tr>
                        </table>
                        <table width="640" border="0" cellpadding="0" cellspacing="0" bgcolor="#0d133d" class="w380" cellmargin="0">
                           <tr>
                              <td style="width: 610px; height: 1px;" class="w380">
                              </td>
                           </tr>
                        </table>
                        <table width="640" border="0" cellpadding="0" cellspacing="0" class="w380" cellmargin="0">
                           <tr>
                              <td style="font-family: arial; font-size:10px; color:#0d133d; line-height:20px;" class="w380 textcenter pleft15 pright15" align="center">
                                 <p><a href="https://www.licitacionescolombia.info" target="_blank" style="text-decoration: underline; font-family: arial; font-size:14px; color:#0d133d; line-height:20px;">Licitaciones en Colombia</a><span style="font-family: arial; font-size:14px; color:#0d133d; line-height:20px;"> | </span><a href="https://www.licitacionesecuador.info" target="_blank" style="text-decoration: underline; font-family: arial; font-size:14px; color:#0d133d; line-height:20px;">Licitaciones en Ecuador</a><span style="font-family: arial; font-size:14px; color:#0d133d; line-height:20px;"> | </span><a href="https://www.licitaciones.info/contacto.html" target="_blank" style="text-decoration: underline; font-family: arial; font-size:14px; color:#0d133d; line-height:20px;">Tienes dudas? contactanos</a></p>
                                 <br>Licitaciones.info monitorea y centraliza las oportunidades de negocio con el sector publico, mixto y privado para ti o tu empresa<br>
                                 Copyright © 2017 licitaciones.info, Todos los derechos reservados.<br>
                                 Licitaciones.info S.A.S Carrera 18 # 71-72 Manizales Colombia<br>
                                 Tel: +57 6 8903340 Cel: +57 310 3708276<br>
                                 E-mail: servicioalcliente@licitaciones.info <br>        
                                 <a href="https://www.licitaciones.info" style="font-family: arial; font-size:12px; color:#0d133d; line-height:12px; text-decoration: underline;" class="pleft15 pright15">www.licitaciones.info</a>
                                 Enviado a: {{ $Nombre }} al e-mail: {{ $Email }} <br>
                                 Para editar sus preferencias de alertas al correo electrónico haga <a href="{LinkUnsuscribe}" target="_blank">clic aqui.</a><br>
                                 Este e-mail ha sido creado sin acentos intencionalmente para una correcta visualizacion.
                              </td>
                           </tr>
                        </table>
                        <!-- spacer  -->
                        <table width="640" border="0" cellpadding="0" cellspacing="0" class="w380" cellmargin="0">
                           <tr>
                              <td style="width: 610px; height: 20px;" class="w380">
                              </td>
                           </tr>
                        </table>
                     </td>
                  </tr>
               </table>
         </tr>
      </table>
   </body>
</html>