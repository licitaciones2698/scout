
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may beginaasd adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example-component', require('./components/ExampleComponent.vue'));
Vue.component('busqueda', require('./components/Busqueda.vue').default);
Vue.component('crear-token',require('./components/passport/CrearTokenPassword.vue'));
Vue.component('crear-cliente',require('./components/passport/CrearTokenCliente.vue'));
Vue.component('crear-personal',require('./components/passport/CrearTokenPersonal.vue'));
Vue.component('crear-facil',require('./components/passport/CrearTokenFacil.vue'));


// los de passport oficiales
Vue.component(
    'passport-clients',
    require('./components/passport/Clients.vue')
);

Vue.component(
    'passport-authorized-clients',
    require('./components/passport/AuthorizedClients.vue')
);

Vue.component(
    'passport-personal-access-tokens',
    require('./components/passport/PersonalAccessTokens.vue')
);

const app = new Vue({
    el: '#app'
});
